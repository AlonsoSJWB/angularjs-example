(function(){

    let  app = angular.module('gallery', []);
    
    app.directive("productGallery", function () {
        return {
            restrict: "E",
            templateUrl: "templates/product-gallery.html",
            controller: function () {
    
                this.current = 0;
                console.log(this.current)
                this.setCurrent = function (i) {
                    this.current = i || 0;
                    console.log("setCurrent", i)
                };
    
                this.isSet = function (i) {
                    return i === this.current;
                    console.log("isSet", i)
                };
            },
            controllerAs: "gallery"
        };
    })

})();