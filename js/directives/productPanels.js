(function(){

    let  app = angular.module('tabs', []);
    
    app.directive("productTabs", function () {
        return {
            restrict: "E",
            templateUrl: "templates/product-panels.html",
            controller: function () {

                this.current = 0;

                this.setCurrent = function (i) {
                    this.current = i || 0;
                };

                this.isSet = function (i) {
                    return i === this.current;
                };
                console.log("tabs")
            },
            controllerAs: "tab"
        };
    })

})();

