(function () {
    let app = angular.module('fStore', ['ui.router', 'ui.router.state.events', 'ui.bootstrap', 'app', 'landing', 'navbar', 'footer', 'products', 'product', 'gallery', 'filters', 'tabs', 'tabsDetail', 'descripttab', 'specstab']);

  
    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

      $stateProvider
      .state('landing', {
        url: '/landing',
        templateUrl: 'templates/landing.html',
        controller: 'Landing'
      })
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/main-panel.html',
        controller: 'App'
      })
      .state('app.products', {
        url: '/products',
        views: {
          'content': {
            templateUrl: 'templates/products.html',
            controller: 'Products'
          }
        }
      })
      .state('app.product', {
        url: '/products/:_id',
        views: {
          'content': {
            templateUrl: 'templates/product.html',
            controller: 'Product'
          }
        }
      })

      $urlRouterProvider.otherwise('/landing');
      $locationProvider.html5Mode(false);
  
    }]);

  })();