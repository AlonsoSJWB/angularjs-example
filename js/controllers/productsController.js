(function(){

    let  app = angular.module('products', []);
    
    app.controller('Products', ['$scope', '$state', '$http', function ($scope, $state, $http) {
        let store = this;
        store.products = [];
        
        $http.get("./dummy/gems.json").then(function (data) {
            console.info(data.data.gems)
            store.products = data.data.gems;
        });

        store.go = function(_id){
            console.log(_id)
            $state.go('app.product', {
                _id: _id //selectedItem and id is defined
            })
        }

        
    }])
})();