(function(){
    const app = angular.module('fStore', [])

    app.controller('StoreController',[ "$http", function($http){
        let store = this;
        store.products = [];
        
        $http.get("./dummy/gems.json").then(function (data) {
            console.info(data)
            store.products = data.data;
        });
    }])

    app.directive("navbar", function () {
        return {
            restrict: "E",
            templateUrl: "templates/navbar.html"
        };
    })

    app.directive("landing", function () {
        return {
            restrict: "E",
            templateUrl: "templates/landing.html"
        };
    })

    app.directive("footerb", function () {
        return {
            restrict: "E",
            templateUrl: "templates/footer.html"
        };
    })


})();